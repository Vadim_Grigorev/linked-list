import java.util.Collection;
import java.util.Objects;

public class LinkedList<E> implements List<E>, Deque<E> {
  private Node first;
  private Node last;
  private int size;

  public LinkedList() {
  }

  public LinkedList(Collection<E> collection) {
    collection.forEach(v -> add(size() - 1, v));
  }

  public E get(int index) {
    return getNode(index).getValue();
  }

  public int size() {
    return size;
  }

  public boolean isEmpty() {
    return size() == 0;
  }

  public boolean replace(int index, E value) {
    if(index > size() - 1) {
      return false;
    }

    getNode(index).setValue(value);
    return true;
  }

  public boolean remove(int index) {
    if (index >= size()) {
      return false;
    }

    Node node = getNode(index);
    size--;

    if (node.equals(first)) {
      first = first.getNext();
      first.setPrev(null);
      return true;
    }

    if (node.equals(last)) {
      last = last.getPrev();
      last.setNext(null);
      return true;
    }
    
    node.getPrev().setNext(node.getNext());
    node.getNext().setPrev(node.getPrev());
    
    return true;
  }

  public boolean remove (E value) {
    int size = size();

    for (int i = 0; i < size(); i++ ) {
      if (getNode(i).getValue().equals(value)) {
        remove(i);
        i--;
      }
    }

    return size != size();
  }


  public boolean add (int index, E value) {

    if(index - 1 > size()) {
      return false;
    }

    Node node = new Node(value, null, null);
    size++;

    if (first == null) {
      first = node;
      return true;
    }

    if (last == null) {
      if(index == 0) {
        last = first;
        first = node;
        first.setNext(last);
        last.setPrev(first);
        return true;
      }

      last = node;
      last.setPrev(first);
      first.setNext(node);
      return true;
    }

    Node current = first;
    Node prev = null;

    for (int i = 0; i < index; i++) {
      prev = current;
      current = current.getNext();
    }

    if (current == null) {
      node.setPrev(last);
      last.setNext(node);
      last = node;
      return true;
    }

    node.setNext(current);
    node.setPrev(prev);

    current.setPrev(node);

    if (prev == null) {
      first = node;
      return true;
    }

    prev.setNext(node);

    return true;
  }

  public void add(E value) {
    add(size(),value);
  }
  public void addAll(Collection<Collection<E>> collection) {
    collection.stream().
        flatMap(Collection::stream).
        forEach( v -> add (size - 1, v));
  }

  public boolean offer(E value) {
    int size = size();
    add(0, value);
    return size == size();
  }

  public boolean offerFirst (E value) {
    return offer(value);
  }

  public boolean offerLast(E value) {
    int size = size();
    add(size(), value);
    return size == size();
  }

  public E poll() {
    Node node = getNode(size()-1);
    remove(size() - 1);
    return node.getValue();
  }

  public E pollFirst() {
    Node node = getNode(0);
    remove(0);
    return node.getValue();
  }

  public E pollLast() {
    return poll();
  }

  private Node getNode(int index) {
    validate(index);
    
    Node current = first;

    for (int i = 0; i != index; i++) {
      current = current.getNext();
    }

    return current;
  }

  private void validate(int index) throws IndexOutOfBoundsException {
    if (index >= size || index < 0) {
      throw new IndexOutOfBoundsException("Specified index is not exist in list");
    }

  }

  private class Node {
    private E value;
    private Node prevNode;
    private Node nextNode;

    public Node(E value,Node prevNode, Node nextNode)  {
      this.value = value;
      this.prevNode = prevNode;
      this.nextNode = nextNode;
    }

    public void setValue(E value) {
      this.value = value;
    }

    public void setPrev(Node prevNode) {
      this.prevNode = prevNode;
    }

    public void setNext(Node nextNode) {
      this.nextNode = nextNode;
    }

    public E getValue() {
      return this.value;
    }

    public Node getPrev() {
      return this.prevNode;
    }

    public Node getNext() {
      return this.nextNode;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      Node node = (Node) o;
      return value.equals(node.value) && Objects.equals(prevNode, node.prevNode)
          && Objects.equals(nextNode, node.nextNode);
    }

    @Override
    public int hashCode() {
      return Objects.hash(value, prevNode, nextNode);
    }
  }

}
