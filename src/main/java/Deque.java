public interface Deque<E> extends Queue<E> {
  E pollFirst();

  E pollLast();

  boolean offerFirst(E e);

  boolean offerLast(E e);
}
