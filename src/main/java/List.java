import java.util.Collection;

public interface List<E> {
  E get(int i);

  int size();

  boolean isEmpty();

  boolean replace(int index, E e);

  boolean add(int index, E e);

  void add(E e);

  void addAll(Collection<Collection<E>> c);

  boolean remove(int i);

  boolean remove(E e);
}
